<?php

/**
 * Description of Temperature
 *
 * @author Mazhar
 */
class Temperature {

    public function fahrenheit_to_celsius($value) {
        $celsius = 5 / 9 * ($value - 32);
        return $celsius;
    }

    public function celsius_to_fahrenheit($value) {
        $fahrenheit = $value * 9 / 5 + 32;
        return $fahrenheit;
    }

}
