<?php
require './Temperature.php';
$obj = new Temperature($_POST['value']);
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Temperature Converter</title>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    </head>
    <body>
        <section>
            <div class="container">
                <header><h3 class="text-center">Temperature Converter</h3></header>
                <hr>
                <form class="form-group text-center form-inline" action="" method="post">
                    <select class="form-control" name="first_temp_type_name">
                        <option value="fahrenheit">Fahrenheit</option>
                        <option value="celsius">Celsius</option>
                    </select>

                    <label class="form-group">Enter Value :</label>
                    <input id="val" value="<?php if (isset($_POST['value'])) echo $_POST['value']; ?>" class="form-control" type="text" name="value" required="">
                    <select class="form-control" name="second_temp_type_name">
                        <option value="fahrenheit">Fahrenheit</option>
                        <option value="celsius">Celsius</option>
                    </select>
                    <input class="form-control" type="submit" name="submit" value="Convert">
        

                </form>
                <div id="message" class="text-center">
                    <?php
                    if (isset($_POST['submit'])) {
                        $first_temp_type_name = $_POST['first_temp_type_name'];
                        $second_temp_type_name = $_POST['second_temp_type_name'];
                        $value = $_POST['value'];
                        if (($first_temp_type_name == 'fahrenheit') && ($second_temp_type_name == 'celsius')) {
                            $celsious = $obj->fahrenheit_to_celsius($value);
                            echo "$value Fahrenheit  = $celsious Celsious";
                        } if (($first_temp_type_name == 'celsius') && ($second_temp_type_name == 'fahrenheit')) {
                            $fahrenheit = $obj->celsius_to_fahrenheit($value);
                            echo "$value Celsious  = $fahrenheit Fahrenheit";
                        } if (($first_temp_type_name == 'fahrenheit') && ($second_temp_type_name == 'fahrenheit')) {
                            echo "Convertion is invalid.";
                        } if (($first_temp_type_name == 'celsius') && ($second_temp_type_name == 'celsius')) {

                            echo "Convertion is invalid.";
                        }
                    }
                    ?>

                </div>

            </div>
        </section>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            setTimeout(function () {
                $('#message').fadeOut('slow');
                $('#val').val('');
            }, 8000);

        </script>



    </body>
</html>
